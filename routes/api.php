<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatBoxController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    Route::post('/auth/register', [AuthController::class, 'register']);
    Route::post('/auth/login', [AuthController::class, 'login']);
    Route::post('/auth/logout', [AuthController::class, 'logout']);
     #API CHANGE PASSWORD
     Route::post('/forgot-password','UsersController@sendEmailResetPassword');
     Route::post('/check-token-expired','UsersController@checkExpiredToken');
     Route::post('/check-token-exist','UsersController@checkExistToken');

    Route::group(['middleware' => 'jwtAuth'], function()
    {
        Route::post('/change-password','UsersController@updatePassword');
        Route::post('/change-avatar','UsersController@updateAvatar');
        Route::put('/update-info','UsersController@updateInfoUser');

        Route::get('person/profile/{userId}', [UsersController::class, 'listProfile']);
        Route::get('person/posts', [PostController::class, 'listPostPerson']);
        Route::post('person/post', [PostController::class, 'store']);
        Route::delete('person/post/{id}', [PostController::class, 'deletePost']);

        Route::get('page-side/posts',[PostController::class, 'listPostPersonAndFriend']);

        Route::get('/add-friend/{friendId}',[UsersController::class, 'addFriendForever']);
        Route::get('/confirm-friend/{friendId}',[UsersController::class, 'confirmFriendForever']);
        Route::get('/cancel-friend/{friendId}',[UsersController::class, 'cancelFriendForever']);
        Route::get('/search-friend/{keyword}',[UsersController::class, 'searchFriendForever']);
        Route::get('/list-friend',[UsersController::class, 'listFriendForever']);

        Route::post('post/comment/{id}', [PostController::class, 'addComment']);
        Route::post('post/comment/reply/{commentId}', [PostController::class, 'addRFeplyComment']);
        Route::delete('post/comment/remove/{commentId}', [PostController::class, 'removeComment']);

        Route::post('post/like', [PostController::class, 'likePost']);

        Route::post('/chatbox', [ChatBoxController::class, 'addChatbox']);
        Route::post('/add-message', [ChatBoxController::class, 'addMessage']);
        Route::get('/list-room', [ChatBoxController::class, 'listRooms']);
        Route::get('/list-message', [ChatBoxController::class, 'listMessageRoom']);

        Route::get('show-profile', [UsersController::class, 'getMe']);






    });

    Route::get('/queue', function(){
        \Artisan::call('queue:listen');
    });

});
