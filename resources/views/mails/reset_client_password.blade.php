Xin chào, {{$contact_name}} <br>
<br>
Khách hàng đang thực hiện reset mật khẩu 。<br>
Xin vui lòng thực hiện click vào link bên dưới để thay đổi mật khẩu。<br>
<br>
<a target="_blank" href="{{$app_url}}/reset-password?token={{$token}}">{{$app_url}}/reset-password?token={{$token}}</a><br>
<br>
なお、もしお心当たりがない場合、速やかに運営会社までご連絡ください。<br>
<br>

Xin cảm ơn.
