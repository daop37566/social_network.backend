<?php
namespace App\Services;

use App\Constants\Common;
use App\Constants\FriendStatus;
use App\Models\Post;
use App\Models\User;
use App\Repositories\PostRepository;
use Illuminate\Support\Facades\Storage;
use Str;

class PostService
{
    protected $postRepo;

    public function __construct(
        PostRepository $postRepo
    ){
        $this->postRepo = $postRepo;
    }


    /**
     * Show detail Post Person
     * @param $id
     * @return $channel
     * **/
    public function showPostPerson($id)
    {
        $channel = $this->postRepo->model->where('author_id', $id)->latest()->get();
        $data = $channel;
        foreach($channel as $key => $post){
            $data[$key]['image'] = config('app.app_url') ."". $post->image;
        }

        return $data;
    }

    public function listPostPersonAndFriend($user){
        $userId = $user->id;
        $userPost = $user->where('id', $userId)->first();
        $postPage = $userPost->userFriends()->where('user_status', FriendStatus::ACCEPT_FRIEND)
        ->select('user_friend_id', 'user_id')
        ->groupBy('user_friend_id', 'user_id')
        ->get();

        $data = collect([]);
        // $dataPost = $this->showPostPerson($userId);
        foreach($postPage as $post){
            $items =  $this->postRepo->model->whereIn('author_id', [$post->user_friend_id, $userId])->get();
            foreach($items as $key => $post){
                $items[$key]['author'] = User::find($post->author_id);
                $items[$key]['image'] = config('app.app_url') ."". $post->image;
            }
            $data = $data->concat($items);
        }

        return $data;
    }

    /**
     *  list Post Person
     * @param $id
     * @return $postsPerson
     * **/
    public function postListPerson($userId)
    {
        $postsPerson = $this->showPostPerson(($userId));
        if($postsPerson){
            foreach($postsPerson as $key => $post){
                $postsPerson[$key]['author'] = User::find($post->author_id);
            }
        }
        return $postsPerson;
    }

    public function createPost($data, $authorId){
        $file = $data['image'] ?? null;
        if($file){
            $file_name = '/images/' . strval(strtotime(now())) . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put($file_name, file_get_contents($file));
        }
        $data['image'] = $file_name ?? null;
        $data['author_id'] = $authorId ?? null;
        $data['slug'] = \Str::slug($data['content']);
        $data['created_at'] = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');
        $data['updated_at'] = \Carbon\Carbon::now('Asia/Ho_Chi_Minh');

        $this->postRepo->model->create($data);

        return true;
    }


    /**
     * Delete Post Person
     * @param $id, $authorId
     * @return bool
     * **/
    public function deletePost($id, $authorId){
        $post = Post::where('id', $id)->where('author_id', $authorId)->first();
        if ($post){
            Post::whereId($id)
                ->with(['comments' => function($q){
                    $q->delete();
                }])
                ->first();
            if (Storage::disk('public')->exists($post->image)) {
                Storage::disk('public')->delete($post->image);
            }

            return $post->delete();
        }

        return false;
    }

     /**
     * Check exist user
     * @param $id
     * @return boolean
     * **/
    public function existPost($id)
    {
        $post = Post::where('id', $id)->exists();
        if($post){
            return true;
        }

        return false;
    }


    /**
     * add comment Post
     * @param $data $id
     * return $data
     * **/
    public function createComment($data, $id, $authorId)
    {
        $postItem = $this->postRepo->model->find($id);
        if($postItem){
            $dataNew = $postItem->comments()->create([
                'author_id' => $authorId,
                'body' => $data['body']
            ]);

            $time = config('app.locale') == 'en' ? 'Now' : 'Bây giờ';
            $dataNew['time'] = $time;

            return $dataNew;
        }

        return null;
    }


 /**
     * like Post
     * @param $data $userId
     * return $data
     * **/
    public function likePost($data, $userId)
    {
        $postLike = \DB::table('post_like')->where([
            ['user_id', $userId],
            ['post_id', $data['post_id']]
        ]);

        if(!$postLike->exists()){

            $data = [
                'user_id' => $userId,
                'post_id' => (int)$data['post_id'],
                'status' => Common::STATUS_TRUE
            ];

            \DB::table('post_like')->insert($data);

            return ['message' => 'Đã Yêu Thích'];
        }

        $postLike->delete();

        return ['message' => 'Đã Bỏ Yêu Thích'];

    }
}
