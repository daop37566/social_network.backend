<?php
namespace App\Services;

use App\Repositories\CommentRepository;

class CommentService
{
    protected $commentRepo;

    public function __construct(
        CommentRepository $commentRepo
    ){
        $this->commentRepo = $commentRepo;
    }


    /**
     * add comment Post
     * @param $data $id
     * return $data
     * **/
    public function replyComment($data, $commentId, $authorId)
    {
        $commentItem = $this->commentRepo->model->find($commentId);
        if($commentItem){
            $dataReplyNew = $commentItem->comments()->create([
                'author_id' => $authorId,
                'body' => $data['body']
            ]);

            $time = config('app.locale') == 'en' ? 'Now' : 'Bây giờ';
            $dataReplyNew['time'] = $time;

            return $dataReplyNew;
        }

        return null;
    }

    /**
     * delete comment Post
     * @param $data $id
     * return $data
     * **/
    public function deleteComment($commentId, $authorId)
    {

        $commentItem = $this->commentRepo->model->find($commentId);

        if($authorId == $commentItem->author_id ){
            $checkComment = $this->commentRepo->model->where('commentable_id', $commentId);
            if($commentId AND $checkComment->exists()){
                $checkComment->delete();
            }
            $commentItem->delete();

            return true;
        }

        return false;
    }


    public function filterDataComment($data)
    {

    }

}
