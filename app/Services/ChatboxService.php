<?php
namespace App\Services;

use DB;
use App\Repositories\MessageRepository;
use App\Repositories\RoomRepository;

class ChatBoxService
{
    protected $messageRepo;
    protected $roomRepo;

    public function __construct(
        MessageRepository $messageRepo,
        RoomRepository $roomRepo
    ){
        $this->messageRepo = $messageRepo;
        $this->roomRepo = $roomRepo;
    }


    public function addChatBox($userId, $data)
    {
        $dataRr = [
            'usersRe' => json_encode([$data['friend_id'], $userId]),
        ];
        $dataRoom = [
            'users' => json_encode([$userId, $data['friend_id']]),
        ];

        $roomExists = \DB::table('rooms')->where(function($q) use($dataRoom, $dataRr){
            $q->where('users', $dataRr['usersRe']);
            $q->orWhere('users', $dataRoom['users']);
        })->exists();
        dd($roomExists);

        if($roomExists->exists()){
            return $roomExists->first();
        }
        $roomId = \DB::table('rooms')->insertGetId($dataRoom);
        $room = \DB::table('rooms')->where('id', $roomId)->first();
        return $room;
    }

    public function addMessage($userId, $data)
    {
        $message = [
            'user_id' => $userId,
            'room_id' => $data['room_id'],
            'content' => $data['content'],
            'created_at' => \Carbon\Carbon::now('Asia/Ho_Chi_Minh'),
            'updated_at' => \Carbon\Carbon::now('Asia/Ho_Chi_Minh'),

        ];
        $messageId = \DB::table('messages')->insertGetId($message);

        $data = [];
        $newMessage = \DB::table('messages')->where('id', $messageId)->first();
        $data['newMessage'] = $newMessage;
        return $data;
    }

    public function listRooms($userId)
    {
        $dataMess = \DB::table('messages')->where('user_id', $userId);
        $dataRooms = [];
        if($dataMess->exists()){
            $dataMessH = $dataMess->latest()->select('room_id')->groupBy('room_id')->get();
            foreach($dataMessH as $mess){
                $data['rooms'] = $mess->room_id;
                $data['message'] = $this->messageRepo->model->where('room_id', $mess->room_id)->latest()->first();
                array_push($dataRooms, $data);
            }
        }


        return $dataRooms;
    }


    public function listMessageRoom($userId, $roomId)
    {
        $data = $this->roomRepo->model->find($roomId)->first();

        if($data){
            $dataMess = $data->messages()->get();
            return $dataMess;
        }

        return [];
    }
}
