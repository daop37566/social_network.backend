<?php
namespace App\Services;

use App\Constants\FriendStatus;
use App\Events\CustomerOrder;
use App\Models\User;

use App\Jobs\SendMailResetPasswordJob;
use App\Models\UserFriend;
use App\Notifications\SendRAddFriend;
use App\Notifications\UserFollowed;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Storage;
use Hash;
use Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class UserService
{

    protected $userRepo;

    public function __construct(
        UserRepository $userRepo
    ){
        $this->userRepo = $userRepo;
    }

    /**
     * Check exist user
     * @param $id
     * @return boolean
     * **/
    public function existUser($id)
    {
        $user = User::where('id', $id)->exists();
        if($user){
            return true;
        }

        return false;
    }

    /**
     * Create remember mail
     * @param $email, $token
     * @return void
     * **/
    public function createRememberMail($email, $token)
    {
        User::where('email', $email)
            ->update([
                'remember_token' => $token,
                'email_at' => Carbon::now()
            ]);
    }


     /**
     * Check expired token
     * @param $token
     * @return boolean
     * **/
    public function checkExpiredT($token){
        $startTime = User::where('remember_token', $token)->get('email_at')
                            ->first()
                            ->email_at;
        $endTime = Carbon::parse($startTime)->addHours(1);
        if(Carbon::now() < $endTime){
            return true;
        }

        return false;
    }

    /**
     * Check exist token
     * @param $token
     * @return boolean
     * **/
    public function checkExistToken($token)
    {
        if(User::where('remember_token', $token)){
            return true;
        }

        return false;
    }

    /**
     * Change password
     * @param $token, $password
     * @return boolean
     * **/
    public function changePassword($userId, $password)
    {
        if($this->existUser($userId)){
            User::where('id', $userId)
                ->update([
                    'password' => Hash::make($password),
                    'remember_token' => null
                ]);
            return true;
        }

        return false;
    }

    /**
     * Update password login
     * @param $password, $email
     * @return $status
     * **/
    public function updatePasswordLogin($password, $email)
    {
        return User::where('email', $email)
                ->update([
                    'password' => $password,
                    'remember_token' => null
                ]);
    }

    /**
     * Update password verify
     * @param @oldPass, $userPass, $newPass, $email
     * @return boolean
     * **/
    public function updatePasswordVerify($oldPass, $userPass, $newPass, $email)
    {
        if(Hash::check($oldPass, $userPass)){
            return $this->updatePasswordLogin($newPass, $email);
        }

        return false;
    }

    /**
     * Send email to reset staff / super admin password
     * @param $email
     * @return boolean
     * **/
    public function sendMailToReset($email)
    {
        $resetPasswordJob = null;
        $templateName = null;
        $token = Str::random(100);
        $user = User::where('email', $email)->first();
        $emailInfo = [
            'token' => $token,
            'app_url' => config('app.app_url')
        ];


        $emailInfo['contact_name'] = $user->name ?? "No name";
        $templateName = "reset_client_password";

        $this->createRememberMail($email, $token);
        SendMailResetPasswordJob::dispatch($email, $emailInfo, $templateName);

        return true;
    }



    public function changeAvatar($user, $avatar)
    {
        if (isset($avatar)) {
            if (Storage::disk('public')->exists($user->avatar)) {
                Storage::disk('public')->delete($user->avatar);
            }
            $file = $avatar;
            $file_name = '/images/' . strval(strtotime(now())) . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put($file_name, file_get_contents($file));
            if($this->existUser($user->id)){
                return User::where('id', $user->id)
                    ->update([
                        'avatar' => $file_name,
                    ]);
            }
        }

        return false;
    }


    public function addFriend($user, $friendId){
        $userFriendExists =  $user->userFriends()->where('user_friend_id', $friendId)->exists();
        if(!$userFriendExists){
            $user->userFriends()->create([
                'user_friend_id' => $friendId,
                'user_status' => FriendStatus::COMFILM_FRIEND,
                'person_send_friend' => FriendStatus::SENDER_FR
            ]);

            $userF = User::where('id', $friendId)->first();
            $userF->userFriends()->create([
                'user_friend_id' => $user->id,
                'user_status' => FriendStatus::COMFILM_FRIEND,
                'person_send_friend' => FriendStatus::RECEIVE_FR
            ]);

            Notification::send($user, new SendRAddFriend($userF));
            return true;
        }
        return false;
    }

    public function confirmFriend($user, $friendId){
        $userFriendExists = $user->userFriends()
        ->where([
            ['user_friend_id', $friendId],
            ['user_status', FriendStatus::COMFILM_FRIEND],
            ['person_send_friend', FriendStatus::RECEIVE_FR],
        ]);

        if($userFriendExists->exists()){
            $userFriendExists->update([
                'user_status' => FriendStatus::ACCEPT_FRIEND,
                'person_send_friend' => FriendStatus::SENDER_FR
            ]);

            $userF = User::where('id', $friendId)->first();
            $userF->userFriends()->where('user_friend_id', $user->id)->update([
                'user_status' => FriendStatus::ACCEPT_FRIEND,
            ]);

            return true;
        }

        return false;
    }


    public function cancelFriend($user, $friendId){
        $userFriendExists = $user->userFriends()
        ->where([
            ['user_friend_id', $friendId],
            ['user_status', FriendStatus::COMFILM_FRIEND],
        ]);

        if($userFriendExists->exists()){
            $userFriendExists->delete();

            $userF = User::where('id', $friendId)->first();
            $userF->userFriends()->where('user_friend_id', $user->id)->delete();

            return true;
        }

        return false;
    }

    public function searchFriend($user, $keyword)
    {
        $data = $this->userRepo->model->when(!empty($keyword), function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%');
        })->select('id', 'name', 'avatar', 'email')->get();

        if($data){
            foreach($data as $key => $userU){
                $data[$key]['avatar'] = config('app.app_url') ."". $userU->avatar;
                $userBB = UserFriend::where([
                    ['user_friend_id', $userU->id],
                    ['user_id', $user->id]
                ])->first();
                if(isset($userBB) && $userBB){
                    $data[$key]['status_friend'] = 2;
                }else{
                    $data[$key]['status_friend'] = 1;
                }
            }
        }

        return $data;
    }


    public function listInfoFriend($userId){
        $auth = $this->userRepo->model->find($userId);
        $userFrs = $auth->userFriends()->where('user_status', FriendStatus::ACCEPT_FRIEND)->get();
        $data = collect([]);
        if($userFrs){
            foreach($userFrs as $userFr){
                $user = $this->userRepo->model->find($userFr->user_friend_id);
                $user['avatar'] = config('app.app_url') ."". $user->avatar;
                $data->push($user);
            }
        }

        return $data;
    }

    public function updateInfoUser($userId, $data){
        $this->userRepo->model->find($userId)->update([
            'gender' => $data['gender'],
            'birthday_me' => $data['birthday_me'],
            'phone_number' => $data['phone_number']
        ]);

        return $this->detailUser($userId);

    }


    /**
     * Get detail user
     * @param $userId
     * @return $data
     * **/
    public function detailUser($userId)
    {
        $data = $this->userRepo->model->where('id', $userId);
        if($data->exists()){
            $data = $data->select('id', 'name', 'email', 'phone_number', 'avatar','gender', 'birthday_me')
            ->firstOrFail();
        }
        $data->avatar = config('app.app_url') ."". $data->avatar;
        $data->birthday_me = \Carbon\Carbon::parse($data->birthday_me)->format('d/m/Y');

        return $data;
    }

}
