<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;

class UserFollowed extends Notification implements ShouldQueue, ShouldBroadcastNow
{
    use Queueable;

    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $follower;

    public function __construct(User $follower)
    {
        $this->follower = $follower;
    }

    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toBroadcast($notifiable)
    {
        dd($notifiable);
        return [
            'notifiable' => $notifiable,
            'follower_id' => $this->follower->id,
            'follower_name' => $this->follower->name,
        ];
    }

}

