<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;

class SendRAddFriend extends Notification implements ShouldQueue, ShouldBroadcastNow
{
    use Queueable;

    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $user;

    public function __construct($mess)
    {
        $this->user = $mess;
    }

    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->user,
            'user_name' => $this->user,
        ];
    }

    public function toBroadcast($notifiable)
    {
        dump($notifiable);
        return [
            'notifiable' => $notifiable,
            'data' => $this->user,
        ];
    }

}

