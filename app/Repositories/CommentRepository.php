<?php
namespace App\Repositories;

use App\Models\Comment;
use App\Repositories\BaseRepository;

class CommentRepository extends BaseRepository
{
    public $model;

    public function getModel()
    {
        return Comment::class;
    }
}
