<?php
namespace App\Repositories;

use App\Models\Message;
use App\Repositories\BaseRepository;

class MessageRepository extends BaseRepository
{
    public $model;

    public function getModel()
    {
        return Message::class;
    }
}
