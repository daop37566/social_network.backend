<?php
namespace App\Repositories;

use App\Models\Post;
use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository
{
    public $model;

    public function getModel()
    {
        return Post::class;
    }
}
