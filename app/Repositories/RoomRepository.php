<?php
namespace App\Repositories;

use App\Models\Room;
use App\Repositories\BaseRepository;

class RoomRepository extends BaseRepository
{
    public $model;

    public function getModel()
    {
        return Room::class;
    }
}
