<?php
namespace App\Constants;

class FriendStatus
{
    const STRANGER = 0; // Chưa kết bạn
    const COMFILM_FRIEND = 1; // Chờ xác nhận
    const ACCEPT_FRIEND = 2; // Đã là bạn bè

    const DEFAULT_FR = 0;
    const RECEIVE_FR = 1; // Người nhận lời mời
    const SENDER_FR = 2; // Người gửi lời mời
}
