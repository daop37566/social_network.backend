<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use App\Models\User;

class UserIdRequest extends ApiRequest
{

    protected $userId;


    public function __construct()
    {
        $this->userId = request()->userId;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => [
                'required',
                function($attribute, $value, $fail)
                {
                    $userExists = User::whereId($this->userId);
                    if(!$userExists->exists()){
                        $fail('Người dùng không tồn tại.');
                    }
                }
            ]
        ];
    }

    public function messages()
    {
        return [
            'userId.required' => 'Vui lòng nhập ID',
            'userId.exists' => "Succcess............",
        ];
    }
}
