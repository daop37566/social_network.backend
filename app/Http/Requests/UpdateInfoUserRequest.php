<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;

class UpdateInfoUserRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gender' => 'numeric|between:0,2',
            'birthday_me' => 'date_format:Y/m/d',
            'phone_number' => 'required|regex:/(0)[0-9]/|not_regex:/[a-z]/|min:9',
        ];
    }

    public function messages()
    {
        return [
            'gender.numeric' => 'Sai định dạng',
            'birthday_me.date_format' => 'Sai Định Dạng',
            'phone_number.required' => 'Vui lòng nhập Phone',
            'phone_number.regex' => 'Sai định dạng',
            'phone_number.not_regex' => 'Không chứa chữ cái.',
            'phone_number.min' => 'Tối thiểu 9 kí tự'
        ];
    }
}
