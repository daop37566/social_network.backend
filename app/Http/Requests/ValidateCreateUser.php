<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;

class ValidateCreateUser extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:20|regex:/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}/',
            'confirm_password' => 'required|same:password'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'Name not null',
            'email.required' => 'Email not null',
            'email.email' => 'Wrong email format',
            'email.unique' => 'Email has been used',
            'password.required' => 'Password not null',
            'password.min' => 'Password contain at least 6 characters',
            'password.max' => 'Password contain at most 20 characters',
            'password.regex' => 'Wrong password requirement (must contain at least one number, lowercase and uppercase letter)',
            'confirm_password.required' => 'Confirm password not null',
            'confirm_password.same' => 'Confirm password and password must be same'
        ];
    }
}
