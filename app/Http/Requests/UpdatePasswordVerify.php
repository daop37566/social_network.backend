<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;

class UpdatePasswordVerify extends ApiRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'verify_password' => 'required|min:8|max:255|regex:/^([A-Za-z0-9\d$!^(){}?\[\]<>~%@#&*+=_-]+)$/',
            'password' => 'required|min:8|max:255|regex:/^([A-Za-z0-9\d$!^(){}?\[\]<>~%@#&*+=_-]+)$/',
            'password_confirmation' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'verify_password.required' => 'Vui lòng nhập trường này.',
            'verify_password.min' => 'Tối thiểu 8 kí tự',
            'verify_password.max' => 'Tối đa 25 kí tự',
            'verify_password.regex' => 'Sai định nghĩa.',

            'password.required' => 'Vui lòng nhập trường này.',
            'password.min' => 'Tối thiểu 8 kí tự',
            'password.max' => 'Tối đa 25 kí tự',
            'password.regex' => 'Sai định nghĩa.',

            'password_confirmation.required' => 'Vui lòng nhập trường này.',
            'password_confirmation.same' => 'Xác thực mật khẩu không giống.',
        ];
    }
}
