<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_current'=>'required|min:8|max:255|regex:/^([A-Za-z0-9\d$!^(){}?\[\]<>~%@#&*+=_-]+)$/',
            'password'=>'required|confirmed|min:8|max:255|regex:/^([A-Za-z0-9\d$!^(){}?\[\]<>~%@#&*+=_-]+)$/',
            // 'confirm_password' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'password.required' => "Vui lòng nhập field này.",
            'password.confirmed' =>'Mật khẩu xác thực không đúng',
            'password.min' => 'Tối thiểu 6 kí tự',
            'password.max' => 'Tối đa 20 kí tự',
            'password.regex' => 'Không đúng định dạng',
            'password_current.required' => "Vui lòng nhập field này.",
            'password_current.confirmed' =>'Mật khẩu xác thực không đúng',
            'password_current.min' => 'Tối thiểu 6 kí tự',
            'password_current.max' => 'Tối đa 20 kí tự',
            'password_current.regex' => 'Không đúng định dạng',
        ];
    }
}
