<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ApiRequest;
use App\Models\Post;

class AddCommentPostRequest extends ApiRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required|max:255'
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
