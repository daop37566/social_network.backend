<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmailRequest extends ApiRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email,deleted_at,NULL|max:50|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Vui lòng nhập mail',
            'email.regex' => 'Không đúng định dạng mail',
            'email.email' => 'Không đúng định dạng mail',
            'email.exists' => 'Mail đã tồn tại',
            'email.max' => 'Vượt qá tối đa kí tự.',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = $this->respondSuccess([
            'message' => __('messages.user.validation.email.exists')
        ]);

        throw new HttpResponseException($response);
    }
}
