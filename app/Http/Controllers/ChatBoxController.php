<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ChatBoxService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Http\Response;

class ChatBoxController extends Controller
{
    protected $chatboxService;
    protected $user;

    public function __construct(
        ChatBoxService $chatboxService
    ){
        $this->chatboxService = $chatboxService;
        $this->user = Auth::user();
    }

     /**
     * add chatbox
     * @param $request
     * @return $data
     * **/

     public function addChatbox(Request $request)
     {
         $userId = $this->user->id;
        $data = $request->all();
        DB::beginTransaction();
        try {
            $messData = $this->chatboxService->addChatBox($userId, $data);
            if($messData){
                DB::commit();
                return $this->respondSuccess([
                    'message' => 'Gửi tin nhắn thành công'
                ]);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, 'Không thể gửi tin nhắn.');
        }
     }


     public function addMessage(Request $request)
     {
        $userId = $this->user->id;
        $data = $request->all();
        DB::beginTransaction();
        try {
            $messData = $this->chatboxService->addMessage($userId, $data);
            if($messData){
                DB::commit();
                return $this->respondSuccess($messData);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, 'Không thể gửi tin nhắn.');
        }
     }

     public function listRooms()
     {
        $userId = $this->user->id;

        $messData = $this->chatboxService->listRooms($userId);
        if($messData){

            return $this->respondSuccess($messData);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Không tin nhắn rooms.');
     }


     public function listMessageRoom(Request $request)
     {
        $userId = $this->user->id;
        $messData = $this->chatboxService->listMessageRoom($userId, $request->room_id);

        if($messData){

            return $this->respondSuccess($messData);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Không thể hiển thị Message.');
     }

}
