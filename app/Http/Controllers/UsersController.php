<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Hash;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;
use App\Services\UserTokenService;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\EmailRequest;
use App\Http\Requests\EmailTokenRequest;
use App\Http\Requests\UpdateStaffInfoRequest;
use App\Http\Requests\UpdatePasswordVerify;
use App\Http\Requests\NewEmailRequest;
use App\Constants\Role;
use App\Events\CustomerOrder;
use App\Http\Requests\ChangeAvatarRequest;
use App\Http\Requests\UpdateInfoUserRequest;
use App\Http\Requests\UserIdRequest;
use App\Models\User;
use App\Notifications\SendRAddFriend;
use App\Notifications\SendRequestAddFriend;
use App\Notifications\UserFollowed;
use Illuminate\Support\Facades\Notification;

class UsersController extends Controller
{

    protected $userService;
    protected $userTokenService;
    private $user;

    public function __construct(
        UserService $userService,
        UserTokenService $userTokenService
    ){
        $this->userService = $userService;
        $this->userTokenService = $userTokenService;
        $this->user = Auth::user();
    }

    public function getUserCurrent(Request $request)
    {
        $user = Auth::user();
        return $this->respondSuccess($user);
    }


     /**
     * send email Reset Password
     * @param $request
     * @return $data
     * **/
    public function sendEmailResetPassword(EmailRequest $request)
    {
        if($this->userService->sendMailToReset($request->email)){
            return $this->respondSuccess([
                "message" => 'Kiểm tra mail để xác nhận thay đổi.'
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Vui lòng thử lại.');
    }

     /**
     * Update Password
     * @param $request
     * @return $data
     * **/
    public function updatePassword(ChangePasswordRequest $request)
    {
        $userId = $this->user->id;
        $data = $request->all();
        if(!Hash::check( $data['password_current'], $this->user->password )){
            return $this->respondError(Response::HTTP_BAD_REQUEST, 'Mật khẩu của bạn không đúng.');
        }

        if( $data['password_current'] == $data['password']){
            return $this->respondError(Response::HTTP_BAD_REQUEST, 'Không được trùng với mật khẩu hiện tại.');
        }
        $status = $this->userService
                       ->changePassword($userId, $data['password']);
        if($status){
            return $this->respondSuccess([
                'message' =>  'Thay đổi mật khẩu thành công.'
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Vui lòng thử lại.');
    }

    public function checkExpiredToken(EmailTokenRequest $request)
    {
        if($this->userService->checkExpiredToken($request->token)){
            return $this->respondSuccess([
                'message' => __('messages.user.token_success')
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.user.token_fail'));
    }

    public function getMe()
    {
        if(Auth::check()){
            $id = Auth::user()->id;
            $data = $this->userService->detailUser($id);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.user.detail_fail'));
    }

    public function updatePasswordWithVerify(UpdatePasswordVerify $request)
    {
        $oldPassword = $request->verify_password;
        $newPassword = $request->password;
        $userPassword = Auth::user()->password;
        $email = Auth::user()->email;

        $status = $this->userService->updatePasswordVerify(
            $oldPassword, $userPassword, Hash::make($newPassword), $email
        );

        if($status){
            $this->userTokenService->destroyUserToken(Auth::user()->id);
            Auth::logout();
            return $this->respondSuccess([
                'message' => __('messages.user.password_success')
            ]);
        }
        else if($status === false){
            return $this->respondError(Response::HTTP_UNPROCESSABLE_ENTITY, [
                "verify_password" => [__('messages.user.password_verify_fail')]
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.user.password_fail'));
    }

    public function checkExistToken(EmailTokenRequest $request)
    {
        if($this->userService->checkExistToken($request->token)){
            return $this->respondSuccess([
                'message' => __('messages.user.token_success')
            ]);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.user.token_fail')
        );
    }

    public function inviteNewAdminStaff(NewEmailRequest $request)
    {
        $requestEmail = $request->email;
        \DB::beginTransaction();
        try {
            if($this->userService->inviteNewAdminStaff($requestEmail)){
                \DB::commit();

                return $this->respondSuccess([
                    'message' => __('messages.mail.send_success')
                ]);
            }
            \DB::rollback();

            return $this->respondError(
                Response::HTTP_BAD_REQUEST, __('messages.mail.send_fail')
            );

        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
            return $this->respondError(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }
    }

    public function upadateStaffAdmin(UpdateStaffInfoRequest $request)
    {
        $userId = Auth::user()->id;
        $requestData = $request->all();
        $data = $this->userService->staffAdminInfoUpdate($requestData, $userId);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('messages.user.update_fail')
        );
    }

    public function updateAvatar(ChangeAvatarRequest $request)
    {
        $user = $this->user;
        $data = $request->all();
        $status = $this->userService
                       ->changeAvatar($user, $data['avatar']);
        if($status){
            return $this->respondSuccess([
                'message' =>  'Thay đổi ảnh đại diện thành công.'
            ]);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Vui lòng thử lại.');
    }

    public function addFriendForever($friendId){
        if(Auth::check()){
            $user = Auth::user();
            $data = $this->userService->addFriend($user, $friendId);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.user.detail_fail'));
    }

    public function confirmFriendForever($friendId){
        if(Auth::check()){
            $user = Auth::user();
            $data = $this->userService->confirmFriend($user, $friendId);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.user.detail_fail'));
    }


    public function cancelFriendForever($friendId){
        if(Auth::check()){
            $user = Auth::user();
            $data = $this->userService->cancelFriend($user, $friendId);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.user.detail_fail'));
    }

     /**
     * search Friend
     * @param $keyword
     * @return $data
     * **/
    public function searchFriendForever($keyword){
        $user = $this->user;
        if($user){
            $data = $this->userService->searchFriend($user, $keyword);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Xin lỗi không tìm thấy.');
    }

    /**
     * list Friend Accept
     * @param null
     * @return $data
     * **/
    public function listFriendForever(){
        $userId = $this->user->id;
        if($userId){
            $data = $this->userService->listInfoFriend($userId);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Xin lỗi không tìm thấy.');
    }

    public function updateInfoUser(UpdateInfoUserRequest $request){
        $userId = $this->user->id;
        $data = $request->all();
        if($userId){
            $data = $this->userService->updateInfoUser($userId, $data);
        }

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Xin lỗi không tìm thấy.');
    }


    public function listProfile($userId){

        $data = $this->userService->detailUser($userId);

        if($data){
            return $this->respondSuccess($data);
        }

        return $this->respondError(Response::HTTP_BAD_REQUEST, 'Xin lỗi không tìm thấy.');
    }

}
