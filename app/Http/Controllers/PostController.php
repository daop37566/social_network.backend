<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCommentPostRequest;
use App\Services\CommentService;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;

class PostController extends Controller
{
    protected $postService;
    protected $commentService;
    protected $user;

    public function __construct(
        PostService $postService,
        CommentService $commentService
    ){
        $this->postService = $postService;
        $this->commentService = $commentService;
        $this->user = Auth::user();

    }

        /**
     * Get list event
     * @param $request
     * @return $data
     * **/
    public function listPostPerson()
    {
        $userId = $this->user->id;
        $responseData = $this->postService->postListPerson($userId);

        if($responseData){
            return $this->respondSuccess($responseData);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('Faill .........')
        );
    }

    public function listPostPersonAndFriend()
    {
        $user = $this->user;
        $responseData = $this->postService->listPostPersonAndFriend($user);
        if($responseData){
            return $this->respondSuccess($responseData);
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, __('Faill .........')
        );
    }

    public function store(Request $request)
    {
        $authorId = $this->user->id;
        DB::beginTransaction();
        $postData = $this->postService->createPost($request->all(), $authorId);
        try {
            if($postData){
                DB::commit();
                return $this->respondSuccess($postData);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.create_fail'));
        }
    }

    public function deletePost($id)
    {
        $authorId = $this->user->id;
        $post = $this->postService->deletePost($id, $authorId);
        if ($post) return $this->respondSuccess(['messages' => 'Bài đăng đã được xóa.']);

        if(!$this->postService->existPost($id)){
            return $this->respondError(
                Response::HTTP_NOT_FOUND, 'Không tìm thấy bài đăng'
            );
        }

        return $this->respondError(
            Response::HTTP_BAD_REQUEST, 'Xóa không thành công.'
        );
    }


    public function addComment(AddCommentPostRequest $request, $id){
        $authorId = $this->user->id;
        DB::beginTransaction();
        $postData = $this->postService->createComment($request->all(), $id, $authorId);
        try {
            if($postData){
                DB::commit();
                return $this->respondSuccess($postData);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.create_fail'));
        }
    }

    public function addRFeplyComment(AddCommentPostRequest $request, $commentId){
        $authorId = $this->user->id;
        DB::beginTransaction();
        $postData = $this->commentService->replyComment($request->all(), $commentId, $authorId);
        try {
            if($postData){
                DB::commit();
                return $this->respondSuccess($postData);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.create_fail'));
        }
    }

    public function removeComment($commentId){
        $authorId = $this->user->id;
        DB::beginTransaction();
        $postData = $this->commentService->deleteComment($commentId, $authorId);
        try {
            if($postData){
                DB::commit();
                return $this->respondSuccess($postData);
            }
            DB::rollback();
            return $this->respondError(
                Response::HTTP_BAD_REQUEST, 'Bạn không có quyền xóa bình luận.'
            );
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.create_fail'));
        }
    }


      /**
     * like Post
     * @param $request
     * @return $data
     * **/

     public function likePost(Request $request){
        $userId = $this->user->id;
        DB::beginTransaction();
        $postData = $this->postService->likePost($request->all(), $userId);
        try {
            if($postData){
                DB::commit();
                return $this->respondSuccess($postData);
            }
            DB::rollback();
        } catch (\Exception $th) {
            DB::rollback();
            throw $th;
            return $this->respondError(Response::HTTP_BAD_REQUEST, __('messages.event.create_fail'));
        }
     }
}
