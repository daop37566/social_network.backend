<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;

class User extends AuthJWT
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    protected $fillable = [
        'id', 'name', 'email', 'password', 'avatar', 'lasted_login', 'phone_number', 'birthday_me', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     /**
     * Password need to be all time encrypted.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = \Hash::make($password);
    }

    /**
     * Password need to be all time encrypted.
     *
     * @param string $password
     */
    public function setBirdayMeAttribute($value)
    {
        $this->attributes['birthday_me'] = date_format($value,"Y/m/d");
    }


    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id', 'id');
    }

    public function userTokens()
    {
        return $this->hasMany(UserToken::class, 'user_id', 'id');
    }

    public function userFriends()
    {
        return $this->hasMany(UserFriend::class, 'user_id', 'id');
    }
}
