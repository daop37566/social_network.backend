<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFriend extends Model
{
    protected $table = 'user_friend';
    protected $fillable = ['id', 'user_id', 'user_friend_id', 'user_status', 'person_send_friend'];

    /**
     * Get the user that owns the UserToken
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
