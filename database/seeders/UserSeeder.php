<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Duy Phương',
            'email' => 'daop37566@gmail.com',
            'avatar' => '/images/default_avatar.png',
            'password' => Hash::make('Duyphuong@2506')
        ]);
    }
}
