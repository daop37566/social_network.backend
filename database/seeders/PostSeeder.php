<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Str;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 2 ; $i++){
            DB::table('posts')->insert([
                'author_id' => 2,
                'content' => 'I love You .',
                'image' => '/images/default_avatar.png',
                'slug' => Str::slug('I miss You'),
            ]);
        }
    }
}
